package goutils

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"syscall"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
)

func TestMultiServerGracefulShutdown(t *testing.T) {
	logger := logrus.New()
	server := MultiServer(logger)
	h := http.NewServeMux()
	h.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		server.logger.Info("Handling request to / ...")
		time.Sleep(2 * time.Second)
		server.logger.Info("Done handling request")
		fmt.Fprintf(w, "phew that took a while")
	})
	handler := http.Handler(h)

	host := "127.0.0.1"
	port := 19908

	go server.Add("Testo Server", &handler, port, host)
	go func() {
		res, err := http.Get(fmt.Sprintf("http://%s:%d/", host, port))
		if err != nil || res.StatusCode != 200 {
			t.Fatal("Got error while requesting", err)
		}
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Fatal("Got error while reading response body", err)
		}
		logger.Infof("Got response %d %s", res.StatusCode, string(body))
	}()
	go func() {
		time.Sleep(100 * time.Millisecond)
		syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
	}()
	<-server.Shutdown
	server.logger.Info("Shutdown done")
}
