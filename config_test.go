package goutils

import (
	"os"
	"testing"
)

type TestConfigSchema struct {
	Port     int
	Host     string
	Logger   LoggerConfig
	Postgres PostgresConfig
}

func TestLoadConfig(t *testing.T) {
	os.Setenv("ENV", "test")
	os.Setenv("POSTGRES_HOST", "1.2.3.4")

	testConfigSchema := TestConfigSchema{}
	LoadConfig(&testConfigSchema, ".")

	if testConfigSchema.Host != "0.0.0.0" {
		t.Error("Did not load Host")
	}

	if testConfigSchema.Logger.Formatter != "potato" {
		t.Error("Did not load nested config")
	}

	if testConfigSchema.Postgres.Host != "1.2.3.4" {
		t.Errorf("Expected %s, got %s", "1.2.3.4", testConfigSchema.Postgres.Host)
	}
}
