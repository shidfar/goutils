package goutils

import (
	"testing"

	"github.com/sirupsen/logrus"
)

func TestLoggerConfigGetNotJSON(t *testing.T) {
	loggerConfig := LoggerConfig{
		Formatter: "chicken",
	}

	logger := loggerConfig.Get()
	_, ok := logger.Formatter.(*logrus.TextFormatter)

	if !ok {
		t.Errorf("Incorrect formatter")
	}
}

func TestLoggerConfigGetJSON(t *testing.T) {
	loggerConfig := LoggerConfig{
		Formatter: "JSON",
	}

	logger := loggerConfig.Get()
	_, ok := logger.Formatter.(*logrus.JSONFormatter)

	if !ok {
		t.Errorf("Incorrect formatter")
	}
}
