package goutils

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
)

// MultiServer starts multiple Mux's listening on different ports and hosts,
// then handles graceful shutdown across all servers on sigint.

type multiServer struct {
	running      *sync.WaitGroup
	logger       *logrus.Logger
	shutdownOnce *sync.Once
	Shutdown     chan bool
}

// GracefulShutdownWait specifies how long the server should wait before
// stopping itself, allowing any existing in-flight requests to finish.
//
// This should be set lower than the grace period set in the container
// orchestration. E.g. for iflix Kubernetes cluster, this defaults to 30s before
// k8s will send a SIGKILL to the pods, so the grace period here should be
// under 30s (unless `terminationGracePeriodSeconds` is set to something
// different (probably shouldn't)).
const GracefulShutdownWait = time.Duration(15 * time.Second)

// HTTPServerWriteTimeout is the maximum duration before timing out
// writes of the response. It is reset whenever a new
// request's header is read. Like HTTPServerReadTimeout, it does not
// let Handlers make decisions on a per-request basis.
const HTTPServerWriteTimeout = time.Duration(15 * time.Second)

// HTTPServerReadTimeout is the maximum duration for reading the entire
// request, including the body.
const HTTPServerReadTimeout = time.Duration(15 * time.Second)

// HTTPServerIdleTimeout is the maximum amount of time to wait for the
// next request when keep-alives are enabled. If IdleTimeout
// is zero, the value of ReadTimeout is used. If both are
// zero, ReadHeaderTimeout is used.
const HTTPServerIdleTimeout = time.Duration(60 * time.Second)

func (t *multiServer) Add(name string, handler *http.Handler, port int, host string) {
	addr := fmt.Sprint(host, ":", port)
	t.logger.WithFields(logrus.Fields{"addr": addr}).Info("Starting ", name)
	srv := http.Server{
		Addr:         addr,
		WriteTimeout: HTTPServerWriteTimeout,
		ReadTimeout:  HTTPServerReadTimeout,
		IdleTimeout:  HTTPServerIdleTimeout,
		Handler:      *handler,
	}
	// increment the wait group
	t.running.Add(1)
	idleConnsClosed := make(chan bool)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		sig := <-signalChan
		t.logger.WithFields(logrus.Fields{
			"sig":          sig.String(),
			"graceSeconds": GracefulShutdownWait,
		}).Infof("Received signal, waiting (up to %s) for existing requests to finish...", GracefulShutdownWait)
		ctx, cancel := context.WithTimeout(context.Background(), GracefulShutdownWait)
		defer cancel()
		// Doesn't block if no connections, but will otherwise wait
		// until the timeout deadline.
		if err := srv.Shutdown(ctx); err != nil {
			// Error from closing listeners, or context timeout:
			t.logger.Error("Shutdown failed: ", err)
		} else {
			t.logger.Info("Successfully shut down ", name)
		}
		// decrement the wait group and signal closed
		t.running.Done()
		close(idleConnsClosed)
	}()

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		// Error starting or closing listener:
		t.logger.Error("HTTP server ListenAndServe: ", err)
	}
	<-idleConnsClosed
	// wait for all services to shutdown
	t.running.Wait()
	// close our shutdown channel
	t.shutdownOnce.Do(func() {
		close(t.Shutdown)
	})

}

func MultiServer(logger *logrus.Logger) *multiServer {
	return &multiServer{
		logger:       logger,
		shutdownOnce: &sync.Once{},
		running:      &sync.WaitGroup{},
		Shutdown:     make(chan bool),
	}
}
