package goutils

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
)

// LoadConfig Searches for configs on the `configPaths` and loads them into `config`
func LoadConfig(config interface{}, configPaths ...string) *viper.Viper {
	/* load config from config/{ENV}.toml where ENV
	* is either ENV environment variable or "development"
	*
	* config is a &reference to a struct to hold the unmarshaled config
	 */
	v := viper.New()
	env := os.Getenv("ENV")
	if env == "" {
		env = "development"
	}
	for i := 0; i < len(configPaths); i++ {
		v.AddConfigPath(configPaths[i])
	}
	v.SetConfigName(env)

	// Support automated environment variable overrides, using "_" to join
	// nested properties. Example config:
	//
	// [Postgres]
	// Host = 0.0.0.0
	//
	// Can be overriden with POSTGRES_HOST=1.2.3.4
	v.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	v.SetEnvKeyReplacer(replacer)

	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("could not read config file %s", err))
	}
	err = v.Unmarshal(config)
	if err != nil {
		panic(fmt.Errorf("could not understand config file %s", err))
	}

	return v
}
