package goutils

import (
	"github.com/sirupsen/logrus"
)

// LoggerConfig Config for logger
type LoggerConfig struct {
	Formatter    string
	LoggingLevel string
}

// Get Uses the config and initialises a logger.
// Currently supports `JSON`.
func (l *LoggerConfig) Get() *logrus.Logger {
	// configure logrus logger
	logger := logrus.New()

	if level, err := logrus.ParseLevel(l.LoggingLevel); err != nil {
		logger.Warn(err)
		logger.SetLevel(logrus.InfoLevel)
	} else {
		logger.SetLevel(level)
	}

	if l.Formatter == "JSON" {
		logger.Formatter = &logrus.JSONFormatter{}
	}

	return logger
}
